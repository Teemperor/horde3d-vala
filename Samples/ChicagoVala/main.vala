// *************************************************************************************************
//
// Horde3D
//   Next-Generation Graphics Engine
//
// Sample Application
// --------------------------------------
// Copyright (C) 2006-2011 Nicolas Schulz
//
//
// This sample source file is not covered by the EPL as the rest of the SDK
// and may be used without any restrictions. However, the EPL's disclaimer of
// warranty and liability shall be in effect for this file.
//
// *************************************************************************************************



// Configuration
using GLFW;

const int appWidth = 1024;
const int appHeight = 576;
const bool fullScreen = false;

static Application app;

bool setupWindow( int width, int height, bool fullscreen )
{
	// Create OpenGL window
	if(!glfwOpenWindow( width, height, 8, 8, 8, 8, 24, 8, Mode.WINDOW))
	{
		glfwTerminate();
		return false;
	}

	// Disable vertical synchronization
	glfwSwapInterval( 0 );

	return true;
}


int main()
{

    H3D.root_node = 1;
	// Initialize GLFW
	glfwInit();
	if( !setupWindow( appWidth, appHeight, fullScreen ) ) return -1;
	// Initialize application and engine
	app = new Application();


	app.init();
	app.resize( appWidth, appHeight );


	// Game loop
	while (true)
	{

		// Render
		app.mainLoop (60);
		glfwSwapBuffers();

	}

	// Quit
	//app.release();
	//glfwTerminate();

	//return 0;
}
