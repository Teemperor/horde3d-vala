
public class Particle
{
	public float    px;
	public float    pz;// Current postition
	public float    dx;
	public float    dz;// Destination position
	public float    fx;
	public float    fz;  // Force on particle
	public float    ox;
	public float    oz;  // Orientation vector
	public H3D.Node?  node;
	public float    animTime;


	public Particle() {
		ox = 0;
		oz = 0;
		node = null;
		animTime = 0;
	}
}

public class CrowdSim {
    Particle[] particles = {};

	public CrowdSim () {
	}

	public void init() {
	    // Load character with walk animation
	    H3D.Res characterRes = H3D.add_resource (H3D.ResTypes.SceneGraph, "models/man/man.scene.xml", 0 );
	    H3D.Res characterWalkRes = H3D.add_resource( H3D.ResTypes.Animation, "animations/man.anim", 0 );
	    H3D.Utility.load_resources_from_disk ("../../../../../Binaries/Content");

	    // Add characters
	    for( int i = 0; i < 100; ++i )
	    {
		    Particle p = new Particle ();

		    // Add character to scene and apply animation
		    p.node = H3D.root_node.add_nodes (characterRes);
		    p.node.setup_model_anim_stage (0, characterWalkRes, 0, "", false );

		    // Characters start in a circle formation
		    p.px = Math.sinf ((i / 100.0f) * 6.28f ) * 10.0f;
		    p.pz = Math.cosf ((i / 100.0f) * 6.28f ) * 10.0f;

		    chooseDestination (p);

		    p.node.set_transform (p.px, 0.02f, p.pz, 0, 0, 0, 1, 1, 1 );

		    particles += p;
	    }
    }

	public void update( float fps ) {
	    // Parameters for three repulsion zones
	    float d1 = 0.25f, d2 = 2.0f, d3 = 4.5f;
	    float f1 = 3.0f, f2 = 1.0f, f3 = 0.1f;

	    for( int i = 0; i < particles.length; ++i )
	    {
		    Particle p = particles[i];

		    // Reset force
		    p.fx = 0; p.fz = 0;

		    // Calculate distance to destination
		    float dist = Math.sqrtf( (p.dx - p.px)*(p.dx - p.px) + (p.dz - p.pz)*(p.dz - p.pz) );

		    // If destination not reached walk to destination
		    if( dist > 3.0f )
		    {
			    // Calculate normalized attraction force to destination
			    float afx = (p.dx - p.px) / dist;
			    float afz = (p.dz - p.pz) / dist;

			    p.fx += afx * 0.035f;
			    p.fz += afz * 0.035f;

			    // Repulsion forces from other particles
			    for( int j = 0; j < particles.length; ++j )
			    {
				    if (j == i ) continue;

				    Particle p2 = particles[j];

				    float dist2 = Math.sqrtf( (p.px - p2.px)*(p.px - p2.px) + (p.pz - p2.pz)*(p.pz - p2.pz) );
				    float strength = 0;

				    float rfx = (p.px - p2.px) / dist2;
				    float rfz = (p.pz - p2.pz) / dist2;

				    // Use three zones with different repulsion strengths
				    if( dist2 <= d3 && dist2 > d2 )
				    {
					    float m = (f3 - 0) / (d2 - d3);
					    float t = 0 - m * d3;
					    strength = m * dist2 + t;
				    }
				    else if( dist2 <= d2 && dist2 > d1 )
				    {
					    float m = (f2 - f3) / (d1 - d2);
					    float t = f3 - m * d2;
					    strength = m * dist2 + t;
				    }
				    else if( dist2 <= d1 )
				    {
					    float m = (f1 - f2) / (0 - d1);
					    float t = f2 - m * d1;
					    strength = m * dist2 + t;
				    }

				    p.fx += rfx * strength; p.fz += rfz * strength;
			    }
		    }
		    else
		    {
			    // Choose new destination
			    chooseDestination( p );
		    }

		    // Make movement framerate independent
		    p.fx *= (30 / fps);
		    p.fz *= (30 / fps);
		    float vel = Math.sqrtf( p.fx * p.fx + p.fz * p.fz );

		    // Set new position
		    p.px += p.fx; p.pz += p.fz;

		    // Calculate orientation
		    p.ox = (p.ox + p.fx) / 2;
		    p.oz = (p.oz + p.fz) / 2;

		    // Get rotation from orientation
		    float ry = 0;
		    if( p.oz != 0 ) ry = (float) Math.atan2( p.ox, p.oz );
		    ry *= 180 / 3.1415f;	// Convert from radians to degrees

		    // Update character scene node position
		    p.node.set_transform (p.px, 0.02f, p.pz, 0, ry, 0, 1, 1, 1 );

		    // Update animation
		    p.animTime += vel * 35.0f;
		    p.node.set_model_anim_params (0, p.animTime, 1.0f );
		    p.node.update_model (H3D.ModelUpdateFlags.Animation | H3D.ModelUpdateFlags.Geometry );
	    }
    }

	public void chooseDestination( Particle p ) {
	    // Choose random destination within a circle
	    float ang = ((Random.next_int () % 360) / 360.0f) * 6.28f;
	    float rad = (Random.next_int () % 20) * 1.0f;

	    p.dx = Math.sinf( ang ) * rad;
	    p.dz = Math.cosf( ang ) * rad;
    }
}
