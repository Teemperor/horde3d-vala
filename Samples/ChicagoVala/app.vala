// *************************************************************************************************
//
// Horde3D
//   Next-Generation Graphics Engine
//
// Sample Application
// --------------------------------------
// Copyright (C) 2006-2011 Nicolas Schulz
//
//
// This sample source file is not covered by the EPL as the rest of the SDK
// and may be used without any restrictions. However, the EPL's disclaimer of
// warranty and liability shall be in effect for this file.
//
// *************************************************************************************************

public class Application {

    float x;  // Viewer position and orientation
    float y;
    float z;
    float rx;
    float ry;
    float        velocity;  // Velocity for movement

    int          statMode;

    CrowdSim     crowdSim;

    // Engine objects
    H3D.Res       fontMatRes;
    H3D.Res       panelMatRes;
    H3D.Res       logoMatRes;
    H3D.Res       forwardPipeRes;
    H3D.Res       deferredPipeRes;
    H3D.Node      cam;

    public Application () {

	    x = 15;
	    y = 3;
	    z = 20;
	    rx = -10;
	    ry = 60;
	    velocity = 10.0f;

	    statMode = 0;
	    cam = 0;
    }


    public bool init() {
    	// Initialize engine
	    if( !H3D.init() )
	    {
		    H3D.Utility.dump_messages();
		    return false;
	    }

	    // Set options
	    H3D.set_option (H3D.Options.LoadTextures, 1 );
	    H3D.set_option (H3D.Options.TexCompression, 0 );
	    H3D.set_option (H3D.Options.MaxAnisotropy, 4 );
	    H3D.set_option (H3D.Options.ShadowMapSize, 2048 );
	    H3D.set_option (H3D.Options.FastAnimation, 1 );

	    // Add resources
	    // Pipelines
	    forwardPipeRes = H3D.add_resource ( H3D.ResTypes.Pipeline, "pipelines/forward.pipeline.xml", 0 );
	    deferredPipeRes = H3D.add_resource ( H3D.ResTypes.Pipeline, "pipelines/deferred.pipeline.xml", 0 );
	    // Overlays
	    fontMatRes = H3D.add_resource ( H3D.ResTypes.Material, "overlays/font.material.xml", 0 );
	    panelMatRes = H3D.add_resource ( H3D.ResTypes.Material, "overlays/panel.material.xml", 0 );
	    logoMatRes = H3D.add_resource ( H3D.ResTypes.Material, "overlays/logo.material.xml", 0 );
	    // Shader for deferred shading
	    H3D.Res lightMatRes = H3D.add_resource ( H3D.ResTypes.Material, "materials/light.material.xml", 0 );
	    // Environment
	    H3D.Res envRes = H3D.add_resource ( H3D.ResTypes.SceneGraph, "models/platform/platform.scene.xml", 0 );
	    // Skybox
	    H3D.Res skyBoxRes = H3D.add_resource ( H3D.ResTypes.SceneGraph, "models/skybox/skybox.scene.xml", 0 );

	    // Load resources
	    H3D.Utility.load_resources_from_disk ( "../../../../../Binaries/Content" );

	    // Add scene nodes
	    // Add camera
	    cam = H3D.root_node.add_camera_node ("Camera", forwardPipeRes );
	    //h3dSetNodeParamI( _cam, H3DCamera.OccCullingI, 1 );
	    // Add environment
	    H3D.Node env = H3D.root_node.add_nodes (envRes );
	    env.set_transform (0, 0, 0, 0, 0, 0, 0.23f, 0.23f, 0.23f );
	    // Add skybox
	    H3D.Node sky = H3D.root_node.add_nodes (skyBoxRes );
	    sky.set_transform (0, 0, 0, 0, 0, 0, 210, 50, 210 );
	    sky.set_flags (H3D.NodeFlags.NoCastShadow, true );
	    // Add light source
	    H3D.Node light = H3D.root_node.add_light_node ("Light1", lightMatRes, "LIGHTING", "SHADOWMAP" );
	    light.set_transform (0, 20, 50, -30, 0, 0, 1, 1, 1 );
	    light.set_param_f (H3D.Light.RadiusF, 0, 200 );
	    light.set_param_f (H3D.Light.FovF, 0, 90 );
	    light.set_param_i (H3D.Light.ShadowMapCountI, 3 );
	    light.set_param_f (H3D.Light.ShadowSplitLambdaF, 0, 0.9f );
	    light.set_param_f (H3D.Light.ShadowMapBiasF, 0, 0.001f );
	    light.set_param_f (H3D.Light.ColorF3, 0, 0.9f );
	    light.set_param_f (H3D.Light.ColorF3, 1, 0.7f );
	    light.set_param_f (H3D.Light.ColorF3, 2, 0.75f );

	    crowdSim = new CrowdSim ();
	    crowdSim.init();

	    return true;
    }

    public void mainLoop( float fps ) {

		crowdSim.update (30);
	    // Set camera parameters
	    cam.set_transform (x, y, z, rx, ry, 0, 1, 1, 1 );

	    // Show logo
	    /*float ww = (float)cam.get_param_i (H3D.Camera.ViewportWidthI) /
	                     (float)cam.get_param_i (H3D.Camera.ViewportHeightI);
	    float ovLogo[] = { ww-0.4f, 0.8f, 0, 1,  ww-0.4f, 1, 0, 0,  ww, 1, 1, 0,  ww, 0.8f, 1, 1 };
	    H3D.show_overlays ( ovLogo, 1.0f, 1.0f, 1.0f, 1.0f, logoMatRes, 0 );
*/
	    // Render scene
	    cam.render ();

	    // Finish rendering of frame
	    H3D.finalize_frame ();

	    // Remove all overlays
	    H3D.clear_overlays ();

	    // Write all messages to log file
	    H3D.Utility.dump_messages ();
    }
    public void release() {
	    H3D.release();
    }
    public void resize( int width, int height ) {
	    // Resize viewport
	    cam.set_param_i (H3D.Camera.ViewportXI, 0 );
	    cam.set_param_i (H3D.Camera.ViewportYI, 0 );
	    cam.set_param_i (H3D.Camera.ViewportWidthI, width );
	    cam.set_param_i (H3D.Camera.ViewportHeightI, height );

	    // Set virtual camera parameters
	    cam.setup_camera_view (45.0f, (float)width / height, 0.1f, 1000.0f );
	    deferredPipeRes.resize_pipeline_buffers (width, height );
	    deferredPipeRes.resize_pipeline_buffers (width, height );
    }
}
